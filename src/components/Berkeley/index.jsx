import React, { useState } from "react";
import axios from "axios";
import "./Berkeley.css";
function Berkeley() {
  const [t1, setT1] = useState("");
  const [t2, setT2] = useState("");
  const [t3, setT3] = useState("");
  const [t4, setT4] = useState("");
  const [value, setValue] = useState("Tính toán");
  const [status, setStatus] = useState(true);
  const [message, setMessage] = useState("");
  function handleOnChangeT1(e) {
    setT1(e.target.value);
  }

  function handleOnChangeT2(e) {
    setT2(e.target.value);
  }

  function handleOnChangeT3(e) {
    setT3(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();

    const formValue = {
      date1: t1,
      date2: t2,
      date3: t3,
    };

    console.log(formValue);
    axios
      .post("http://localhost:8080/api/v1/berkeley", formValue)
      .then((res) => {
        const status = res.status;
        if (status === 200) {
          setMessage("Kết quả");
          console.log("Msg: " + message);
          setStatus(false);
        } else {
          setMessage("Đầu vào sai");
          console.log("Msg: " + message);
          setStatus(true);
        }
        const data = res.data;
        setT1(data.p1);
        setT2(data.p2);
        setT3(data.p3);
        setT4(data.synchronizedTime);
      })
      .catch((error) => console.log(error));
    console.log("Msg: " + message);
  }
  return (
    <div>
      <h3>Berkeley</h3>
      <h4>{message}</h4>
      <form className="form">
        <input
          className="form__text"
          type="text"
          value={t1}
          onChange={handleOnChangeT1}
          placeholder="Điều phối"
        />
        <input
          className="form__text"
          type="text"
          value={t2}
          onChange={handleOnChangeT2}
          placeholder="Thành viên"
        />
        <input
          className="form__text"
          type="text"
          value={t3}
          onChange={handleOnChangeT3}
          placeholder="Thành viên"
        />
        <input className="form__text" type="text" value={t4} disabled />
        <input
          type="submit"
          value={value}
          disabled={!status}
          onClick={handleSubmit}
        />
      </form>
    </div>
  );
}

export default Berkeley;
